# from django.db import models
#
# class Portrait(models.Model):
#     GENDER_MALE   = 1
#     GENDER_FEMALE = 2
#
#     GENDER_CHOICES = (
#         (GENDER_MALE, 'Мужской'),
#         (GENDER_FEMALE, 'Женский'),
#     )
#
#     ROLE_PARENT   = 1
#     ROLE_CHILDREN    = 2
#     ROLE_EDUCATOR = 3
#
#     ROLE_CHOICES = (
#         (ROLE_PARENT, 'Родитель'),
#         (ROLE_CHILDREN, 'Ребенок'),
#         (ROLE_EDUCATOR, 'Воспитатель'),
#     )
#
#     INSTITUTION = (
#         (1, 'Учреждение №1'),
#         (1, 'Учреждение №2'),
#         (1, 'Учреждение №3'),
#         (1, 'Учреждение №4'),
#         (1, 'Учреждение №5')
#     )
#
#     user          = models.OneToOneField(User, on_delete=models.CASCADE)
#     gender        = models.SmallIntegerField(choices=GENDER_CHOICES, default=GENDER_MALE, null=True, blank=True)
#     birth_date    = models.DateField(null=True, blank=True)
#     fullname      = models.CharField(max_length=250, null=True, blank=True)
#     fullnameother = models.CharField(max_length=250, null=True, blank=True)
#     job           = models.CharField(max_length=250, null=True, blank=True)
#     Institution   = models.PositiveIntegerField(null=True, blank=True)
#     role_id       = models.SmallIntegerField(choices=ROLE_CHOICES, null=True, blank=True)
#
#
#     class Meta:
#         db_table = "tbl_portraits"
