from django.shortcuts import render
from django.contrib.auth.decorators import login_required

@login_required
def portrait(request):
    return render(request, 'portrait/portrait.html', {})
