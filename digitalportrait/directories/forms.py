from django import forms
from directories.models import Directories

class CreateForm(forms.ModelForm):
    title       = forms.CharField(label=False)
    description = forms.CharField(label=False, widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(CreateForm, self).__init__(*args, **kwargs)

        self.fields['title'].widget.attrs = {
            'class': 'form-control',
            'placeholder': 'Введите заголовок'
        }
        self.fields['description'].widget.attrs = {
            'class': 'form-control',
            'placeholder': 'Введите текст '
        }


    class Meta:
        model = Directories
        fields = '__all__'
