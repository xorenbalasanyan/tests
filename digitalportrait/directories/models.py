from django.db import models
from django.contrib.auth.models import User

class Directories(models.Model):
    user        = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    title       = models.TextField(null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    created     = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = "tbl_directories"
