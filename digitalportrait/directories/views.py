from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from profiles.models import Profiles
from directories.models import Directories
from directories.forms import CreateForm

@login_required
def index(request):
    if request.user.profiles.role_id is not Profiles.ROLE_EDUCATOR:
        return redirect('home')

    if request.method == 'POST':
        form = CreateForm(request.POST)
        if form.is_valid():
            model             = Directories()
            model.title       = form.cleaned_data.get('title')
            model.description = form.cleaned_data.get('description')
            model.user        = request.user
            model.save()
    else:
        form = CreateForm()

    directories = Directories.objects.order_by('-id').all()

    return render(request, 'directories/index.html', {'directories':directories, 'form':form})
