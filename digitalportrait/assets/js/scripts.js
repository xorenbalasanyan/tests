if ($('#cy').length) {
    // (function() {
    //     window.cy = cytoscape({
    //         container: document.getElementById('cy'),
    //
    //         layout: {
    //             name: 'grid',
    //             cols: 3
    //         },
    //
    //         style: [{
    //                 "selector": "node[label]",
    //                 "style": {
    //                     "label": "data(label)"
    //                 }
    //             },
    //
    //             {
    //                 "selector": "edge[label]",
    //                 "style": {
    //                     "label": "data(label)",
    //                     "width": 3
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".top-left",
    //                 "style": {
    //                     "text-valign": "top",
    //                     "text-halign": "left"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".top-center",
    //                 "style": {
    //                     "text-valign": "top",
    //                     "text-halign": "center"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".top-right",
    //                 "style": {
    //                     "text-valign": "top",
    //                     "text-halign": "right"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".center-left",
    //                 "style": {
    //                     "text-valign": "center",
    //                     "text-halign": "left"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".center-center",
    //                 "style": {
    //                     "text-valign": "center",
    //                     "text-halign": "center"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".center-right",
    //                 "style": {
    //                     "text-valign": "center",
    //                     "text-halign": "right"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".bottom-left",
    //                 "style": {
    //                     "text-valign": "bottom",
    //                     "text-halign": "left"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".bottom-center",
    //                 "style": {
    //                     "text-valign": "bottom",
    //                     "text-halign": "center"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".bottom-right",
    //                 "style": {
    //                     "text-valign": "bottom",
    //                     "text-halign": "right"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".multiline-manual",
    //                 "style": {
    //                     "text-wrap": "wrap"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".multiline-auto",
    //                 "style": {
    //                     "text-wrap": "wrap",
    //                     "text-max-width": 80
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".autorotate",
    //                 "style": {
    //                     "edge-text-rotation": "autorotate"
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".background",
    //                 "style": {
    //                     "text-background-opacity": 1,
    //                     "color": "#fff",
    //                     "text-background-color": "#888",
    //                     "text-background-shape": "roundrectangle",
    //                     "text-border-color": "#000",
    //                     "text-border-width": 1,
    //                     "text-border-opacity": 1
    //                 }
    //             },
    //
    //             {
    //                 "selector": ".outline",
    //                 "style": {
    //                     "color": "#fff",
    //                     "text-outline-color": "#888",
    //                     "text-outline-width": 3
    //                 }
    //             }
    //         ],
    //
    //         elements: [{
    //                 data: {
    //                     label: 'top left'
    //                 },
    //                 classes: 'top-left'
    //             },
    //             {
    //                 data: {
    //                     label: 'top center'
    //                 },
    //                 classes: 'top-center'
    //             },
    //             {
    //                 data: {
    //                     label: 'top right'
    //                 },
    //                 classes: 'top-right'
    //             },
    //
    //             {
    //                 data: {
    //                     label: 'center left'
    //                 },
    //                 classes: 'center-left'
    //             },
    //             {
    //                 data: {
    //                     label: 'center center'
    //                 },
    //                 classes: 'center-center'
    //             },
    //             {
    //                 data: {
    //                     label: 'center right'
    //                 },
    //                 classes: 'center-right'
    //             },
    //
    //             {
    //                 data: {
    //                     label: 'bottom left'
    //                 },
    //                 classes: 'bottom-left'
    //             },
    //             {
    //                 data: {
    //                     label: 'bottom center'
    //                 },
    //                 classes: 'bottom-center'
    //             },
    //             {
    //                 data: {
    //                     label: 'bottom right'
    //                 },
    //                 classes: 'bottom-right'
    //             },
    //
    //             {
    //                 data: {
    //                     label: 'multiline manual\nfoo\nbar\nbaz'
    //                 },
    //                 classes: 'multiline-manual'
    //             },
    //             {
    //                 data: {
    //                     label: 'multiline auto foo bar baz'
    //                 },
    //                 classes: 'multiline-auto'
    //             },
    //             {
    //                 data: {
    //                     label: 'outline'
    //                 },
    //                 classes: 'outline'
    //             },
    //
    //             {
    //                 data: {
    //                     id: 'ar-src'
    //                 }
    //             },
    //             {
    //                 data: {
    //                     id: 'ar-tgt'
    //                 }
    //             },
    //             {
    //                 data: {
    //                     source: 'ar-src',
    //                     target: 'ar-tgt',
    //                     label: 'autorotate (move my nodes)'
    //                 },
    //                 classes: 'autorotate'
    //             },
    //             {
    //                 data: {
    //                     label: 'background'
    //                 },
    //                 classes: 'background'
    //             }
    //         ]
    //     });
    // })();
    let cy = cytoscape({
        container: document.getElementById('cy'),

        style: [{
                selector: 'node',
                style: {
                    'background-color': '#69e',
                    'label': 'data(id)',
                }
            },

            {
                selector: 'edge',
                style: {
                    'width': 1,
                    'line-color': '#369',
                    'target-arrow-color': '#369',
                    'target-arrow-shape': 'triangle',
                    'label': 'data(label)',
                    'font-size': '14px',
                    'color': '#777'
                }
            }
        ],

        style: cytoscape.stylesheet()
            .selector('edge')
            .css({
                'width': 3,
                'line-color': '#369',
                'target-arrow-color': '#369',
                'target-arrow-shape': 'triangle',
                'label': 'data(label)',
                'font-size': '14px',
                'color': '#777'
            })
            .selector('node')
            .css({
                'content': 'data(label)',
                'text-valign': 'center',
                'color': 'white',
                'text-outline-width': 2,
                'text-outline-color': '#888',
                'background-color': '#888'
            })
            .selector(':selected')
            .css({
                'background-color': 'black',
                'line-color': 'black',
                'target-arrow-color': 'black',
                'source-arrow-color': 'black',
                'text-outline-color': 'black'
            }),

        layout: {
            name: 'grid',
            rows: 1
        }

    });

    cy.add([{
            group: 'nodes',
            data: {
                id: 'n1',
                name: 'n11',
                label: 'Предметные УУД математика',
            },
            position: {
                x: 50,
                y: 50
            }
        },
        {
            group: 'nodes',
            data: {
                id: 'n2'
            },
            position: {
                x: 1250,
                y: 50
            }
        },
        {
            group: 'nodes',
            data: {
                id: 'n3'
            },
            position: {
                x: 50,
                y: 250
            }
        },
        {
            group: 'nodes',
            data: {
                id: 'n4'
            },
            position: {
                x: 1250,
                y: 250
            }
        },
        {
            group: 'nodes',
            data: {
                id: 'n5'
            },
            position: {
                x: 50,
                y: 450
            }
        },
        {
            group: 'nodes',
            data: {
                id: 'n6'
            },
            position: {
                x: 1250,
                y: 450
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e0',
                source: 'n1',
                target: 'n2',
                label: 7
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e1',
                source: 'n2',
                target: 'n3',
                label: 10
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e2',
                source: 'n1',
                target: 'n6',
                label: 14
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e3',
                source: 'n1',
                target: 'n3',
                label: 9
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e4',
                source: 'n2',
                target: 'n4',
                label: 15
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e5',
                source: 'n3',
                target: 'n4',
                label: 11
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e6',
                source: 'n3',
                target: 'n6',
                label: 2
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e7',
                source: 'n6',
                target: 'n5',
                label: 9
            }
        },
        {
            group: 'edges',
            data: {
                id: 'e8',
                source: 'n5',
                target: 'n4',
                label: 6
            }
        },
    ]);

}
