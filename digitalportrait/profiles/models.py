from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

class Profiles(models.Model):
    GENDER_MALE   = 1
    GENDER_FEMALE = 2

    GENDER_CHOICES = (
        (GENDER_MALE, 'Мужской'),
        (GENDER_FEMALE, 'Женский'),
    )

    ROLE_PARENT   = 1
    ROLE_CHILDREN = 2
    ROLE_EDUCATOR = 3

    ROLE_CHOICES = (
        (ROLE_PARENT, 'Родитель'),
        (ROLE_CHILDREN, 'Ребенок'),
        (ROLE_EDUCATOR, 'Воспитатель'),
    )

    INSTITUTION = (
        (1, 'Учреждение №1'),
        (2, 'Учреждение №2'),
        (3, 'Учреждение №3'),
        (4, 'Учреждение №4'),
        (5, 'Учреждение №5')
    )

    user          = models.OneToOneField(User, on_delete=models.CASCADE)
    gender        = models.SmallIntegerField(choices=GENDER_CHOICES, default=GENDER_MALE, null=True, blank=True)
    birth_date    = models.DateField(null=True, blank=True)
    fullname      = models.CharField(max_length=250, null=True, blank=True)
    fullnameother = models.CharField(max_length=250, null=True, blank=True)
    job           = models.CharField(max_length=250, null=True, blank=True)
    institution   = models.PositiveIntegerField(null=True, blank=True)
    role_id       = models.SmallIntegerField(choices=ROLE_CHOICES, null=True, blank=True)


    class Meta:
        db_table = "tbl_profiles"

@receiver(post_save, sender=User)
def update_user_profiles(sender, instance, created, **kwargs):
    if created:
        Profiles.objects.create(user=instance)
    instance.profiles.save()
