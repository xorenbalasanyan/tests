# Generated by Django 3.0.5 on 2020-05-05 06:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='profiles',
            old_name='Institution',
            new_name='institution',
        ),
    ]
