from django.urls import path
from . import views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('login/', views.profile_login, name='profile_login'),
    path('current/', views.profile_current, name='profile_current'),
    path('registration-children/', views.profile_registration_children, name='profile_registration_children'),
    path('registration-parent/', views.profile_registration_parent, name='profile_registration_parent'),
    path('registration-educator/', views.profile_registration_educator, name='profile_registration_educator'),
    path('logout/', auth_views.LogoutView.as_view(), name='profile_logout'),
]
