from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from profiles.forms import LoginForm, RegistrationParentForm, RegistrationChildrenForm, RegistrationEducatorForm
from profiles.models import Profiles
from django.contrib.auth.decorators import login_required
from django.contrib.auth import update_session_auth_hash

@login_required
def profile_current(request):
    if request.user.profiles.role_id == Profiles.ROLE_PARENT:
        if request.method == 'POST':
            form = RegistrationParentForm(request.POST, instance=request.user)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()
                if form.cleaned_data.get('fullname'):
                    user.profiles.fullname = form.cleaned_data.get('fullname')
                if form.cleaned_data.get('fullnameother'):
                    user.profiles.fullnameother = form.cleaned_data.get('fullnameother')
                user.profiles.role_id = Profiles.ROLE_PARENT
                user.save()
                update_session_auth_hash(request, user)
        else:
            form = RegistrationParentForm(instance=request.user)
        return render(request, 'profiles/registration_parent.html', {'form': form})

    if request.user.profiles.role_id == Profiles.ROLE_CHILDREN:
        if request.method == 'POST':
            form = RegistrationChildrenForm(request.POST, instance=request.user)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()
                if form.cleaned_data.get('fullname'):
                    user.profiles.fullname = form.cleaned_data.get('fullname')
                if form.cleaned_data.get('fullnameother'):
                    user.profiles.fullnameother = form.cleaned_data.get('fullnameother')
                if form.cleaned_data.get('birth_date'):
                    user.profiles.birth_date = form.cleaned_data.get('birth_date')
                if form.cleaned_data.get('gender'):
                    user.profiles.gender = form.cleaned_data.get('gender')
                if form.cleaned_data.get('institution'):
                    user.profiles.institution = form.cleaned_data.get('institution')
                user.profiles.role_id = Profiles.ROLE_CHILDREN
                user.save()
                update_session_auth_hash(request, user)
        else:
            form = RegistrationChildrenForm(instance=request.user)
        return render(request, 'profiles/registration_children.html', {'form': form})

    if request.user.profiles.role_id == Profiles.ROLE_EDUCATOR:
        if request.method == 'POST':
            form = RegistrationEducatorForm(request.POST, instance=request.user)
            if form.is_valid():
                user = form.save()
                user.refresh_from_db()
                if form.cleaned_data.get('fullname'):
                    user.profiles.fullname = form.cleaned_data.get('fullname')
                if form.cleaned_data.get('job'):
                    user.profiles.job = form.cleaned_data.get('job')
                if form.cleaned_data.get('birth_date'):
                    user.profiles.birth_date = form.cleaned_data.get('birth_date')
                if form.cleaned_data.get('gender'):
                    user.profiles.gender = form.cleaned_data.get('gender')
                if form.cleaned_data.get('institution'):
                    user.profiles.institution = form.cleaned_data.get('institution')

                user.profiles.role_id = Profiles.ROLE_EDUCATOR
                user.save()
                update_session_auth_hash(request, user)
        else:
            form = RegistrationEducatorForm(instance=request.user)
        return render(request, 'profiles/registration_educator.html', {'form': form})

    return redirect('home')

def profile_login(request):
    if request.user.is_authenticated:
        return redirect('home')
    form = LoginForm()
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            user = authenticate(username = form.cleaned_data.get('username'), password = form.cleaned_data.get('password'))
            if user is not None and user.is_active:
                login(request, user)
                return redirect('home')

    return render(request, 'profiles/login.html', {'form': form})

def profile_registration_parent(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        form = RegistrationParentForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profiles.role_id       = Profiles.ROLE_PARENT
            user.profiles.fullname      = form.cleaned_data.get('fullname')
            user.profiles.fullnameother = form.cleaned_data.get('fullnameother')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegistrationParentForm()

    return render(request, 'profiles/registration_parent.html', {'form': form})

def profile_registration_children(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        form = RegistrationChildrenForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profiles.role_id       = Profiles.ROLE_CHILDREN
            user.profiles.fullname      = form.cleaned_data.get('fullname')
            user.profiles.fullnameother = form.cleaned_data.get('fullnameother')
            user.profiles.birth_date    = form.cleaned_data.get('birth_date')
            user.profiles.gender        = form.cleaned_data.get('gender')
            user.profiles.institution   = form.cleaned_data.get('institution')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegistrationChildrenForm()

    return render(request, 'profiles/registration_children.html', {'form': form})

def profile_registration_educator(request):
    if request.user.is_authenticated:
        return redirect('/')
    if request.method == 'POST':
        form = RegistrationEducatorForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()
            user.profiles.role_id     = Profiles.ROLE_EDUCATOR
            user.profiles.fullname    = form.cleaned_data.get('fullname')
            user.profiles.job         = form.cleaned_data.get('job')
            user.profiles.birth_date  = form.cleaned_data.get('birth_date')
            user.profiles.gender      = form.cleaned_data.get('gender')
            user.profiles.institution = form.cleaned_data.get('institution')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = RegistrationEducatorForm()

    return render(request, 'profiles/registration_educator.html', {'form': form})
