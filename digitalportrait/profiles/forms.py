from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from profiles.models import Profiles

class RegistrationParentForm(UserCreationForm):
    password1     = forms.CharField(label='Пароль', max_length=32, widget=forms.PasswordInput)
    password2     = forms.CharField(label='Пподтверждение пароля', max_length=32, widget=forms.PasswordInput)
    username      = forms.CharField(label='Логин')
    fullname      = forms.CharField(label='ФИО')
    fullnameother = forms.CharField(label='ФИО ребенка')

    def __init__(self, *args, **kwargs):
        super(RegistrationParentForm, self).__init__(*args, **kwargs)

        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            if kwargs['instance']:
                self.fields['fullname'].initial      = kwargs['instance'].profiles.fullname
                self.fields['fullnameother'].initial = kwargs['instance'].profiles.fullnameother

        self.fields['password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password2'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['fullname'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['fullnameother'].widget.attrs = {
            'class': 'form-control',
        }

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'fullname', 'fullnameother')

class RegistrationChildrenForm(UserCreationForm):
    CHOICES = (('', '--------------------'), (Profiles.GENDER_MALE, 'Мужской'),(Profiles.GENDER_FEMALE, 'Женский'))

    password1     = forms.CharField(label='Пароль', max_length=32, widget=forms.PasswordInput)
    password2     = forms.CharField(label='Пподтверждение пароля', max_length=32, widget=forms.PasswordInput)
    username      = forms.CharField(label='Логин')
    fullname      = forms.CharField(label='ФИО')
    fullnameother = forms.CharField(label='ФИО Родителя')
    gender        = forms.ChoiceField(label='Пол', choices=CHOICES)
    birth_date    = forms.DateField(label='Дата рождения ГГГГ-ММ-ДД', widget=forms.DateInput())
    institution   = forms.ChoiceField(label='Учреждение', choices=Profiles.INSTITUTION)

    def __init__(self, *args, **kwargs):
        super(RegistrationChildrenForm, self).__init__(*args, **kwargs)

        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            if kwargs['instance']:
                self.fields['fullname'].initial      = kwargs['instance'].profiles.fullname
                self.fields['fullnameother'].initial = kwargs['instance'].profiles.fullnameother
                self.fields['gender'].initial        = kwargs['instance'].profiles.gender
                self.fields['birth_date'].initial    = kwargs['instance'].profiles.birth_date
                self.fields['institution'].initial   = kwargs['instance'].profiles.institution

        self.fields['password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password2'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['fullname'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['fullnameother'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['gender'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['birth_date'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['institution'].widget.attrs = {
            'class': 'form-control',
        }

    class Meta:
        model = User
        fields = ('username', 'fullname', 'fullnameother', 'password1', 'password2', 'gender', 'birth_date', 'institution')

class RegistrationEducatorForm(UserCreationForm):
    CHOICES = (('', '--------------------'), (Profiles.GENDER_MALE, 'Мужской'),(Profiles.GENDER_FEMALE, 'Женский'))

    password1   = forms.CharField(label='Пароль', max_length=32, widget=forms.PasswordInput)
    password2   = forms.CharField(label='Пподтверждение пароля', max_length=32, widget=forms.PasswordInput)
    username    = forms.CharField(label='Логин')
    fullname    = forms.CharField(label='ФИО')
    job         = forms.CharField(label='Должность')
    gender      = forms.ChoiceField(label='Пол', choices=CHOICES)
    birth_date  = forms.DateField(label='Дата рождения ГГГГ-ММ-ДД', widget=forms.DateInput())
    institution = forms.ChoiceField(label='Учреждение', choices=Profiles.INSTITUTION)

    def __init__(self, *args, **kwargs):
        super(RegistrationEducatorForm, self).__init__(*args, **kwargs)

        if kwargs.get('instance'):
            initial = kwargs.setdefault('initial', {})
            if kwargs['instance']:
                self.fields['fullname'].initial    = kwargs['instance'].profiles.fullname
                self.fields['job'].initial         = kwargs['instance'].profiles.job
                self.fields['gender'].initial      = kwargs['instance'].profiles.gender
                self.fields['birth_date'].initial  = kwargs['instance'].profiles.birth_date
                self.fields['institution'].initial = kwargs['instance'].profiles.institution

        self.fields['password1'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['password2'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['fullname'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['job'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['gender'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['birth_date'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['institution'].widget.attrs = {
            'class': 'form-control',
        }

    class Meta:
        model = User
        fields = ('username', 'fullname', 'job', 'password1', 'password2', 'gender', 'birth_date', 'institution')

class LoginForm(forms.Form):
    username = forms.CharField(label='Логин')
    password = forms.CharField(label='Пароль', max_length=32, widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget.attrs = {
            'class': 'form-control',
            'autocomplete': 'off',
        }
        self.fields['password'].widget.attrs = {
            'class': 'form-control',
            'autocomplete': 'off',
        }
