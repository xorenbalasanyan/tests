from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='tasks'),
    path('<int:id>/', views.task, name='task'),
    path('subject/<int:id>/<int:subject_id>/', views.subject, name='subject'),
    path('process/<int:id>/', views.process, name='task_process'),
    path('all/', views.all, name='tasks_children'),
]
