from django.db import models
from django.contrib.auth.models import User

class Tasks(models.Model):
    TASK_PERSONAL      = 1
    TASK_COGNITIVE     = 2
    TASK_REGULATORY    = 3
    TASK_COMMUNICATIVE = 4
    TASK_SUBJECT       = 5

    TASKS_CHOICES = (
        (TASK_PERSONAL, 'Личностные УУД'),
        (TASK_COGNITIVE, 'Познавательные УУД'),
        (TASK_REGULATORY, 'Регулятивные УУД'),
        (TASK_COMMUNICATIVE, 'Коммуникативные УУД'),
        (TASK_SUBJECT, 'Предметные УУД'),
    )

    task_id  = models.SmallIntegerField(choices=TASKS_CHOICES, null=True, blank=True)
    children = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        db_table = "tbl_tasks"

class TasksPersonal(models.Model):
    task       = models.ForeignKey(Tasks, on_delete=models.CASCADE)
    discipline = models.TextField(null=True, blank=True)

    class Meta:
        db_table = "tbl_tasks_personal"

class TasksObjective(models.Model):
    OBJECTIVE_MATH    = 1
    OBJECTIVE_RUSSIAN = 2
    OBJECTIVE_READ    = 3
    OBJECTIVE_WORLD   = 4

    OBJECTIVE_CHOICES = (
        (OBJECTIVE_MATH, 'Математика'),
        (OBJECTIVE_RUSSIAN, 'Русския язык'),
        (OBJECTIVE_READ, 'Литературное чтение'),
        (OBJECTIVE_WORLD, 'Окружающий мир'),
    )

    objective_id  = models.SmallIntegerField(choices=OBJECTIVE_CHOICES, null=True, blank=True)
    title         = models.TextField(null=True, blank=True)
    class Meta:
        db_table = "tbl_tasks_objective"

class TasksObjectiveOptions(models.Model):
    objective  = models.ForeignKey(TasksObjective, on_delete=models.CASCADE)
    option     = models.TextField(null=True, blank=True)
    status_id  = models.BooleanField(null=True, blank=True)

    class Meta:
        db_table = "tbl_tasks_objective_options"
