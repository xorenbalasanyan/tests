from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskCreateForm, TaskPersonalCreateForm
from tasks.models import Tasks, TasksPersonal, TasksObjective
from django.contrib.auth.models import User
from profiles.models import Profiles

@login_required
def index(request):
    if request.user.profiles.role_id is not Profiles.ROLE_EDUCATOR:
        return redirect('home')
    if request.method == 'POST':
        form = TaskCreateForm(request.POST)
        if form.is_valid():
            task          = Tasks()
            children      = form.cleaned_data.get('children')
            task.children = User.objects.get(id=children.user_id)
            task.task_id  = form.cleaned_data.get('task_id')
            task.save()
            return redirect('task', id=task.id)
    else:
        form = TaskCreateForm()

    return render(request, 'tasks/index.html', {'form': form})


@login_required
def task(request, id):
    if request.user.profiles.role_id is not Profiles.ROLE_EDUCATOR:
        return redirect('home')
    queryset = Tasks.objects.filter(id=id)
    task = get_object_or_404(queryset)

    if task.task_id is Tasks.TASK_PERSONAL:
        disciplines = TasksPersonal.objects.filter(task=task).order_by('-id').all()
        if request.method == 'POST':
            form = TaskPersonalCreateForm(request.POST)
            if form.is_valid():
                model            = TasksPersonal()
                model.discipline = form.cleaned_data.get('discipline')
                model.task       = task
                model.save()
        else:
            form = TaskPersonalCreateForm()
        return render(request, 'tasks/personal.html', {'task':task, 'form': form, 'disciplines': disciplines})

    if task.task_id is Tasks.TASK_SUBJECT:
        return render(request, 'tasks/subject.html', {'task':task,'model':TasksObjective})


    return render(request, 'tasks/task.html', {})

@login_required
def subject(request, id, subject_id):
    if request.user.profiles.role_id is not Profiles.ROLE_EDUCATOR:
        return redirect('home')

    title    = TasksObjective.OBJECTIVE_CHOICES[subject_id-1][1]
    subjects = TasksObjective.objects.filter(objective_id=subject_id).all()

    return render(request, 'tasks/subject_options.html', {'title':title,'subjects':subjects})

@login_required
def all(request):
    if request.user.profiles.role_id is not Profiles.ROLE_CHILDREN:
        return redirect('home')

    tasks = Tasks.objects.filter(children=request.user).all()

    return render(request, 'tasks/all.html', {'tasks':tasks, 'model': Tasks})

@login_required
def process(request, id):
    queryset = Tasks.objects.filter(id=id)
    task = get_object_or_404(queryset)

    if request.user != task.children:
        return redirect('home')

    if task.task_id is Tasks.TASK_PERSONAL:
        disciplines = TasksPersonal.objects.filter(task=task).order_by('-id').all()
        return render(request, 'tasks/process/personal.html', {'disciplines': disciplines})

    return render(request, 'tasks/task.html', {})
