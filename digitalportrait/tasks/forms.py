from django import forms
from tasks.models import Tasks, TasksPersonal
from profiles.models import Profiles

class ChildrenModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.fullname

class TaskCreateForm(forms.Form):
    task_id  = forms.ChoiceField(label=False, choices=Tasks.TASKS_CHOICES, widget=forms.RadioSelect)
    children = ChildrenModelChoiceField(label='Выбрать ребенка', queryset=Profiles.objects.filter(role_id=Profiles.ROLE_CHILDREN).order_by('-fullname').all(), to_field_name='user_id')

    def __init__(self, *args, **kwargs):
        super(TaskCreateForm, self).__init__(*args, **kwargs)

        self.fields['task_id'].widget.attrs = {
            'class': 'form-control',
        }
        self.fields['children'].widget.attrs = {
            'class': 'form-control',
        }

    class Meta:
        model = Tasks
        fields = '__all__'

class TaskPersonalCreateForm(forms.Form):
    discipline = forms.CharField(label='Введите дисциплину', widget=forms.Textarea)

    def __init__(self, *args, **kwargs):
        super(TaskPersonalCreateForm, self).__init__(*args, **kwargs)
        self.fields['discipline'].widget.attrs = {
            'class': 'form-control',
        }

    class Meta:
        model = TasksPersonal
        fields = '__all__'
